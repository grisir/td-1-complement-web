<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\UtilisateurService;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ControleurUtilisateur extends ControleurGenerique
{

    public static function afficherErreur($messageErreur = "", $controleur = ""): Response
    {
        return parent::afficherErreur($messageErreur, "utilisateur");
    }

    public static function afficherPublications($idUtilisateur): Response
    {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($idUtilisateur);
        if ($utilisateur === null) {
            MessageFlash::ajouter("error", "Login inconnu.");
            return ControleurUtilisateur::rediriger("afficherListe");
        } else {
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            $publications = (new PublicationRepository())->recupererParAuteur($idUtilisateur);
            return ControleurGenerique::afficherTwig("publication/feed.html.twig", ["publications" => $publications]);
        }
    }

    public static function afficherFormulaireCreation(): Response
    {
        return ControleurGenerique::afficherTwig("utilisateur/inscription.html.twig");
    }

    public static function creerDepuisFormulaire(): Response
    {
        $login = $_POST['login'] ?? null;
        $mdp = $_POST['mot-de-passe'] ?? null;
        $email = $_POST['email'] ?? null;
        $donneesPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        try{
            (new UtilisateurService())->creerUtilisateur($login, $mdp, $email, $donneesPhotoDeProfil);
        }
        catch(ServiceException $e){
            MessageFlash::ajouter("error", $e);
            self::rediriger('afficherFormulaireCreation');
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été crée !");
        return self::rediriger('afficherListe');
    }


    public static function afficherFormulaireConnexion(): Response
    {
        return ControleurGenerique::afficherTwig("utilisateur/connexion.html.twig");
    }

    public static function connecter(): RedirectResponse
    {
        if (!(isset($_POST['login']) && isset($_POST['mot-de-passe']))) {
            MessageFlash::ajouter("error", "Login ou mot de passe manquant.");
            return ControleurUtilisateur::rediriger("afficherFormulaireConnexion");
        }
        $utilisateurRepository = new UtilisateurRepository();
        /** @var Utilisateur $utilisateur */
        $utilisateur = $utilisateurRepository->recupererParLogin($_POST["login"]);

        if ($utilisateur == null) {
            MessageFlash::ajouter("error", "Login inconnu.");
            return ControleurUtilisateur::rediriger("afficherFormulaireConnexion");
        }

        if (!MotDePasse::verifier($_POST["mot-de-passe"], $utilisateur->getMdpHache())) {
            MessageFlash::ajouter("error", "Mot de passe incorrect.");
            return ControleurUtilisateur::rediriger("afficherFormulaireConnexion");
        }

        ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
        MessageFlash::ajouter("success", "Connexion effectuée.");
        return ControleurUtilisateur::rediriger("afficherListe");
    }

    public static function deconnecter(): RedirectResponse
    {
        if (!ConnexionUtilisateur::estConnecte()) {
            MessageFlash::ajouter("error", "Utilisateur non connecté.");
            return ControleurUtilisateur::rediriger("afficherListe");
        }
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("success", "L'utilisateur a bien été déconnecté.");
        return ControleurUtilisateur::rediriger("afficherListe");
    }
}
