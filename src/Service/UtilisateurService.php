<?php

namespace TheFeed\Service;

use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService
{
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil){
        if($login == null || $motDePasse == null || $email == null){
            throw new ServiceException("login, mot de passe, ou email manquant.");
        }
        if(strlen($login) < 4 || strlen($login) > 20){
            throw new ServiceException("Le login doit être compris entre 4 et 20 caractères !");
        }
        if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)){
            throw new ServiceException("Mot de passe invalide !");
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
            throw new ServiceException("Le format de l'adresse email est invalide !");
        }
        $utilisateurRepository = new UtilisateurRepository();
        $utilisateur = $utilisateurRepository->recupererParLogin($login);
        if($utilisateur != null){
            throw new ServiceException("Ce login est déjà pris !");
        }
        $utilisateur = $utilisateurRepository->recupererParEmail($email);
        if($utilisateur != null){
            throw new ServiceException("Un compte avec cette adresse mail est déjà enregisté !");
        }
        //Récupération de l'extension de la photo de profil
        $explosion = explode(".", $donneesPhotoDeProfil['name']);
        $fileExtension = end($explosion);
        if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
            throw new ServiceException("La photo de profil n'est pas au bon format !");
        }
        //Enregistrement de la photo de profil
        $pictureName = uniqid() . '.' . $fileExtension;
        $from = $donneesPhotoDeProfil['tmp_name'];
        $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
        move_uploaded_file($from, $to);
        //Chiffrement du mot de passe
        $mdpHache = MotDePasse::hacher($motDePasse);
        //Enregistrement de l'utilisateur
        $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
        $utilisateurRepository->ajouter($utilisateur);
    }

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur{
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($idUtilisateur);
        if(!$autoriserNull && $utilisateur == null){
            throw new ServiceException("L'utilisateur n'existe pas !");
        }
        return $utilisateur;
    }
}