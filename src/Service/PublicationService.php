<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurGenerique;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;

class PublicationService
{
    public function recupererPublications(): array{
         return (new PublicationRepository())->recuperer();
    }

    public function creerPublication($idUtilisateur, $message) : void {
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException("Il faut être connecté pour pouvoir publier un feed !");
        }

        if ($message == null || $message == "") {
            throw new ServiceException("Le message ne peut pas être vide !");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Le message ne peut pas dépaser 250 caractères !");
        }

        $publication = Publication::create($message, $utilisateur);
        (new PublicationRepository())->ajouter($publication);
    }

    public function recupererPublicationsUtilisateur($idUtilisateur){
        $publications = (new PublicationRepository())->recupererParAuteur($idUtilisateur);
        return $publications;
    }
}