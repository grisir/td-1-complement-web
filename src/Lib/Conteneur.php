<?php

namespace TheFeed\Lib;

class Conteneur{

    private static array $listeservices;

    public static function ajouterService(String $nom, $service): void{
        Conteneur::$listeservices[$nom] = $service;
    }

    public static function recupererService(String $nom){
        return Conteneur::$listeservices[$nom];
    }
}